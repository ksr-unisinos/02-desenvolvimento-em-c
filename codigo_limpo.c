#include "stm32h7xx_hal.h"
#include "stdlib.h"
#include "main.h"
#include "uc_config.h"
#include "uc_bus.h"

#define BUFFERSIZE 2
#define UARTBUFFERSIZE 3

typedef struct data_pins_t {
	int ports[8];
	int pins[8];
} data_pins_t;

data_pins_t data_pins = { { GPIOD, GPIOD, GPIOD, GPIOD, GPIOD, GPIOD, GPIOD,
		GPIOD }, { GPIO_PIN_15, GPIO_PIN_14, GPIO_PIN_13, GPIO_PIN_12,
		GPIO_PIN_11, GPIO_PIN_10, GPIO_PIN_9, GPIO_PIN_8 } };

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *UartHandle);

void __init_mcu(void) {
	HAL_Init();
	SystemClock_Config();

	MX_GPIO_Init();
	MX_USART3_UART_Init();
	MX_USB_OTG_FS_PCD_Init();
}

#define UART_TIMEOUT	(500)
#define BUFF_SIZE		(8)

static void uart_envia_recebe(uint8_t *envia, int env_tam, uint8_t *recebe,
		int rec_tam, int timeout) {
	memset(envia, 0, sizeof(envia));
	memset(recebe, 0, sizeof(recebe));
	HAL_UART_Transmit(&huart3, envia, env_tam, timeout);
	HAL_UART_Receive(&huart3, recebe, rec_tam, timeout);
}

static void realiza_leitura() {
	uint8_t address_to_read;

	uart_envia_recebe(string_address, strlen(string_address), answer_pc, 1,
			UART_TIMEOUT);
	address_to_read = strtoul(answer_pc, NULL, 10);
	address_buffer = uc_read_from_bus(address_to_read);

	memset(answer_pc, 0, sizeof(answer_pc));

	snprintf(answer_pc, sizeof(answer_pc), "%d", address_buffer);
	HAL_UART_Transmit(&huart3, string_data, strlen(string_data), UART_TIMEOUT);
	HAL_UART_Transmit(&huart3, answer_pc, 1, UART_TIMEOUT);
}

static void realiza_escrita() {
	uint8_t address_to_write;

	uart_envia_recebe(string_address, strlen(string_address), answer_pc, 1,
			UART_TIMEOUT);
	address_to_write = strtoul(answer_pc, NULL, 10);
	uart_envia_recebe(string_data, strlen(string_data), answer_pc, 1,
			UART_TIMEOUT);
	uc_write_to_bus(answer_pc, address_to_write);
}

int main(void) {
	static const uint8_t *string_message =
			(uint8_t*) "Operation [w/write or r/read]: ";
	static const uint8_t *string_invalid = (uint8_t*) "Invalid operation!\n";
	static const uint8_t *string_address = (uint8_t*) "Address: ";
	static const uint8_t *string_data = (uint8_t*) "Data: ";

	uint8_t answer_pc[BUFF_SIZE] = { 0 };
	uint8_t answer_fpga[BUFF_SIZE] = { 0 };

	enum {
		INICIO, READ, WRITE, ERROR,
	};

	int estado = INICIO;

	__init_mcu();

	while (1) {

		switch (estado) {
		case INICIO:
			HAL_Delay(500);

			uart_envia_recebe(string_message, strlen(string_message), answer_pc,
					1, UART_TIMEOUT);

			switch (answer_pc) {
			case 'r':
			case 'R':
				state = READ;
				break;
			case 'w':
			case 'W':
				state = WRITE;
				break;
			default:
				state = ERROR;
				break;
			}
			break;
		case READ:
			realiza_leitura();
			state = INICIO;
			break;
		case WRITE:
			realiza_escrita()
			state = INICIO;
			break;
		case ERROR:
			break;
		}
	}
}

/**
 * @brief  Rx Transfer completed callback
 * @param  UartHandle: UART handle
 * @retval None
 */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *UartHandle) {
	if (UartHandle == &huart3) {
		uart_callback = 1;
	}
}

