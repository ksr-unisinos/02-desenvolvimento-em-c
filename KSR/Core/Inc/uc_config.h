#ifndef UC_CONFIG_H_
#define UC_CONFIG_H_

#include "stm32f4xx_hal.h"

//Address pins defines
#define addressBit0_Pin GPIO_PIN_15
#define addressBit0_GPIO_Port GPIOE

#define addressBit1_Pin GPIO_PIN_14
#define addressBit1_GPIO_Port GPIOE

#define addressBit2_Pin GPIO_PIN_13
#define addressBit2_GPIO_Port GPIOE

#define addressBit3_Pin GPIO_PIN_12
#define addressBit3_GPIO_Port GPIOE

#define addressBit4_Pin GPIO_PIN_11
#define addressBit4_GPIO_Port GPIOE

#define addressBit5_Pin GPIO_PIN_10
#define addressBit5_GPIO_Port GPIOE

#define addressBit6_Pin GPIO_PIN_9
#define addressBit6_GPIO_Port GPIOE

#define addressBit7_Pin GPIO_PIN_8
#define addressBit7_GPIO_Port GPIOE

//Data pins defines
#define dataBit0_Pin GPIO_PIN_15
#define dataBit0_GPIO_Port GPIOD

#define dataBit1_Pin GPIO_PIN_14
#define dataBit1_GPIO_Port GPIOD

#define dataBit2_Pin GPIO_PIN_13
#define dataBit2_GPIO_Port GPIOD

#define dataBit3_Pin GPIO_PIN_12
#define dataBit3_GPIO_Port GPIOD

#define dataBit4_Pin GPIO_PIN_11
#define dataBit4_GPIO_Port GPIOD

#define dataBit5_Pin GPIO_PIN_10
#define dataBit5_GPIO_Port GPIOD

#define dataBit6_Pin GPIO_PIN_9
#define dataBit6_GPIO_Port GPIOD

#define dataBit7_Pin GPIO_PIN_8
#define dataBit7_GPIO_Port GPIOD

//Write pin defines
#define write_Pin GPIO_PIN_12
#define write_GPIO_Port GPIOB

#endif /* UC_CONFIG_H_ */
