#ifndef UC_BUS_H_
#define UC_BUS_H_

#include "stm32f4xx_hal.h"
#include "uc_config.h"

//Bus read function
uint8_t uc_read_from_bus(uint8_t *address);

//Bus write function
void uc_write_to_bus(uint8_t *address, uint8_t *data);

//Sets data pins as input
void data_bus_input(void);

//Sets data pins as output
void data_bus_output(void);

//For test, sends binary value to terminal
void writesBinary(char value);

#endif /* UC_BUS_H_ */
