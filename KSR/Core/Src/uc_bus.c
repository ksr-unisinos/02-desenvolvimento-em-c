#include "stm32f4xx_hal.h"
#include "uc_config.h"
#include "uc_bus.h"

/**
  * Bus read function
  * Parameters: address
  * Return value: data readed
  */
uint8_t uc_read_from_bus(uint8_t *address){

	uint8_t data = 0;

	uint8_t address_bit_0, address_bit_1, address_bit_2, address_bit_3, address_bit_4, address_bit_5, address_bit_6, address_bit_7;
	uint8_t data_bit_0, data_bit_1, data_bit_2, data_bit_3, data_bit_4, data_bit_5, data_bit_6, data_bit_7;

	//Converts address into 8 bits
	address_bit_0 = (address[0] & 0x01);
	address_bit_1 = ((address[0] & 0x02)>>1);
	address_bit_2 = ((address[0] & 0x04)>>2);
	address_bit_3 = ((address[0] & 0x08)>>3);
	address_bit_4 = ((address[0] & 0x10)>>4);
	address_bit_5 = ((address[0] & 0x20)>>5);
	address_bit_6 = ((address[0] & 0x40)>>6);
	address_bit_7 = ((address[0] & 0x80)>>7);

	//Sets the address pins
	HAL_GPIO_WritePin(addressBit0_GPIO_Port, addressBit0_Pin, address_bit_0);
	HAL_GPIO_WritePin(addressBit1_GPIO_Port, addressBit1_Pin, address_bit_1);
	HAL_GPIO_WritePin(addressBit2_GPIO_Port, addressBit2_Pin, address_bit_2);
	HAL_GPIO_WritePin(addressBit3_GPIO_Port, addressBit3_Pin, address_bit_3);
	HAL_GPIO_WritePin(addressBit4_GPIO_Port, addressBit4_Pin, address_bit_4);
	HAL_GPIO_WritePin(addressBit5_GPIO_Port, addressBit5_Pin, address_bit_5);
	HAL_GPIO_WritePin(addressBit6_GPIO_Port, addressBit6_Pin, address_bit_6);
	HAL_GPIO_WritePin(addressBit7_GPIO_Port, addressBit7_Pin, address_bit_7);

	//Waits 100ms to make sure data is available
	HAL_Delay(100);

	//Reads data pins
	data_bit_0 = HAL_GPIO_ReadPin(dataBit0_GPIO_Port, dataBit0_Pin);
	data_bit_1 = HAL_GPIO_ReadPin(dataBit1_GPIO_Port, dataBit1_Pin);
	data_bit_2 = HAL_GPIO_ReadPin(dataBit2_GPIO_Port, dataBit2_Pin);
	data_bit_3 = HAL_GPIO_ReadPin(dataBit3_GPIO_Port, dataBit3_Pin);
	data_bit_4 = HAL_GPIO_ReadPin(dataBit4_GPIO_Port, dataBit4_Pin);
	data_bit_5 = HAL_GPIO_ReadPin(dataBit5_GPIO_Port, dataBit5_Pin);
	data_bit_6 = HAL_GPIO_ReadPin(dataBit6_GPIO_Port, dataBit6_Pin);
	data_bit_7 = HAL_GPIO_ReadPin(dataBit7_GPIO_Port, dataBit7_Pin);

	data = data_bit_0|(data_bit_1<<1)|(data_bit_2<<2)|(data_bit_3<<3)|(data_bit_4<<4)|(data_bit_5<<5)|(data_bit_6<<6)|(data_bit_7<<7);

	return data;
}

/**
  * Bus write function
  * Parameters: address, write_buffer
  * Return value: None
  */
void uc_write_to_bus(uint8_t *address, uint8_t *data){

	uint8_t address_bit_0, address_bit_1, address_bit_2, address_bit_3, address_bit_4, address_bit_5, address_bit_6, address_bit_7;
	uint8_t data_bit_0, data_bit_1, data_bit_2, data_bit_3, data_bit_4, data_bit_5, data_bit_6, data_bit_7;

	//Converts address into 8 bits
	address_bit_0 = (address[0] & 0x01);
	address_bit_1 = ((address[0] & 0x02)>>1);
	address_bit_2 = ((address[0] & 0x04)>>2);
	address_bit_3 = ((address[0] & 0x08)>>3);
	address_bit_4 = ((address[0] & 0x10)>>4);
	address_bit_5 = ((address[0] & 0x20)>>5);
	address_bit_6 = ((address[0] & 0x40)>>6);
	address_bit_7 = ((address[0] & 0x80)>>7);

	//Sets the address pins
	HAL_GPIO_WritePin(addressBit0_GPIO_Port, addressBit0_Pin, address_bit_0);
	HAL_GPIO_WritePin(addressBit1_GPIO_Port, addressBit1_Pin, address_bit_1);
	HAL_GPIO_WritePin(addressBit2_GPIO_Port, addressBit2_Pin, address_bit_2);
	HAL_GPIO_WritePin(addressBit3_GPIO_Port, addressBit3_Pin, address_bit_3);
	HAL_GPIO_WritePin(addressBit4_GPIO_Port, addressBit4_Pin, address_bit_4);
	HAL_GPIO_WritePin(addressBit5_GPIO_Port, addressBit5_Pin, address_bit_5);
	HAL_GPIO_WritePin(addressBit6_GPIO_Port, addressBit6_Pin, address_bit_6);
	HAL_GPIO_WritePin(addressBit7_GPIO_Port, addressBit7_Pin, address_bit_7);

	//Sets Data pins as Output
	data_bus_output();

    //Converts data into 8 bits
	data_bit_0 = (data[0] & 0x01);
	data_bit_1 = ((data[0] & 0x02)>>1);
	data_bit_2 = ((data[0] & 0x04)>>2);
	data_bit_3 = ((data[0] & 0x08)>>3);
	data_bit_4 = ((data[0] & 0x10)>>4);
	data_bit_5 = ((data[0] & 0x20)>>5);
	data_bit_6 = ((data[0] & 0x40)>>6);
	data_bit_7 = ((data[0] & 0x80)>>7);

	//Sets data pins
	HAL_GPIO_WritePin(dataBit0_GPIO_Port, dataBit0_Pin, data_bit_0);
	HAL_GPIO_WritePin(dataBit1_GPIO_Port, dataBit1_Pin, data_bit_1);
	HAL_GPIO_WritePin(dataBit2_GPIO_Port, dataBit2_Pin, data_bit_2);
	HAL_GPIO_WritePin(dataBit3_GPIO_Port, dataBit3_Pin, data_bit_3);
	HAL_GPIO_WritePin(dataBit4_GPIO_Port, dataBit4_Pin, data_bit_4);
	HAL_GPIO_WritePin(dataBit5_GPIO_Port, dataBit5_Pin, data_bit_5);
	HAL_GPIO_WritePin(dataBit6_GPIO_Port, dataBit6_Pin, data_bit_6);
	HAL_GPIO_WritePin(dataBit7_GPIO_Port, dataBit7_Pin, data_bit_7);

	//Set write = 0
	HAL_GPIO_WritePin(write_GPIO_Port, write_Pin, GPIO_PIN_RESET);

	//Waits 100ms to make sure data was written
	HAL_Delay(100);

	//Set Data pins as Input (default)
	data_bus_input();

	//Set write = 1
	HAL_GPIO_WritePin(write_GPIO_Port, write_Pin, GPIO_PIN_SET);
}


/**
  * Sets all data pins as input
  * Parameters: None
  * Return value: None
  */
void data_bus_input(void){

	GPIO_InitTypeDef GPIO_InitStruct = {0};

	/*Configure GPIO pins : dataBit0_Pin dataBit3_Pin dataBit5_Pin */
 	GPIO_InitStruct.Pin = dataBit0_Pin|dataBit3_Pin|dataBit5_Pin;
 	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
 	GPIO_InitStruct.Pull = GPIO_NOPULL;
 	HAL_GPIO_Init(GPIOD, &GPIO_InitStruct); // alterado para compilar

 	/*Configure GPIO pins : dataBit6_Pin dataBit1_Pin dataBit2_Pin dataBit4_Pin dataBit7_Pin */
 	GPIO_InitStruct.Pin = dataBit6_Pin|dataBit1_Pin|dataBit2_Pin|dataBit4_Pin|dataBit7_Pin;
 	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
 	GPIO_InitStruct.Pull = GPIO_NOPULL;
 	HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);
}

 /**
  * Sets all data pins as output
  * Parameters: None
  * Return value: None
  */
void data_bus_output(void){

	GPIO_InitTypeDef GPIO_InitStruct = {0};

	/*Configure GPIO pins : dataBit0_Pin dataBit3_Pin dataBit5_Pin */
	GPIO_InitStruct.Pin = dataBit0_Pin|dataBit3_Pin|dataBit5_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(GPIOD, &GPIO_InitStruct); // alterado para compilar

	/*Configure GPIO pins : dataBit6_Pin dataBit1_Pin dataBit2_Pin dataBit4_Pin dataBit7_Pin */
	GPIO_InitStruct.Pin = dataBit6_Pin|dataBit1_Pin|dataBit2_Pin|dataBit4_Pin|dataBit7_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);
}
