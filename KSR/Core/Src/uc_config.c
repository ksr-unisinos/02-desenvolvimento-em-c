#include "stm32f4xx_hal.h"
#include "uc_config.h"

/**
  * GPIO Initialization Function
  * Parameters: None
  * Return value: None
  */
void MX_GPIO_Init(void){

  GPIO_InitTypeDef GPIO_InitStruct = {0};

  //GPIO ports clock enable
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();

  //Configures GPIO pins output level
  HAL_GPIO_WritePin(addressBit2_GPIO_Port, addressBit2_Pin, GPIO_PIN_RESET);
  HAL_GPIO_WritePin(addressBit1_GPIO_Port, addressBit1_Pin, GPIO_PIN_RESET);
  HAL_GPIO_WritePin(write_GPIO_Port, write_Pin, GPIO_PIN_SET);
  HAL_GPIO_WritePin(GPIOD, addressBit7_Pin|addressBit6_Pin|addressBit5_Pin, GPIO_PIN_RESET);

  //Configure GPIO pin : addressBit2_Pin
  GPIO_InitStruct.Pin = addressBit2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(addressBit2_GPIO_Port, &GPIO_InitStruct);

  //Configure GPIO pin : addressBit1_Pin
  GPIO_InitStruct.Pin = addressBit1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(addressBit1_GPIO_Port, &GPIO_InitStruct);

  //Configure GPIO pins : dataBit0_Pin dataBit3_Pin dataBit5_Pin
  GPIO_InitStruct.Pin = dataBit0_Pin|dataBit3_Pin|dataBit5_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct); // alterado para compilar

  //Configure GPIO pins : dataBit7_Pin dataBit6_Pin dataBit1_Pin dataBit2_Pin dataBit4_Pin
  GPIO_InitStruct.Pin = dataBit7_Pin|dataBit6_Pin|dataBit1_Pin|dataBit2_Pin|dataBit4_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  //Configure GPIO pin : write_Pin
  GPIO_InitStruct.Pin = write_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(write_GPIO_Port, &GPIO_InitStruct);

  //Configure GPIO pins : addressBit7_Pin addressBit6_Pin addressBit5_Pin
  GPIO_InitStruct.Pin = addressBit7_Pin|addressBit6_Pin|addressBit5_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);
}
