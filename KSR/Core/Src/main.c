#include <stdlib.h>

#include "main.h"
#include "uc_bus.h"

UART_HandleTypeDef huart2;

#define BUFFERSIZE 2
#define UARTBUFFERSIZE 3

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *UartHandle);
int uart_callback = 0;

void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART2_UART_Init(void);

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *UartHandle){

	if (UartHandle == &huart2) {
		uart_callback = 1;
	}
}

int main(void)
{
	char uart_operation_buffer[1] = { 0 };
	char uart_address_buffer[UARTBUFFERSIZE] = { 0 };
	char uart_data_buffer[UARTBUFFERSIZE] = { 0 };

	int read = 0;
	int write = 0;
	int ready = 1;

	uint8_t address_buffer[BUFFERSIZE] = { 0 };
	uint8_t read_buffer[BUFFERSIZE] = { 0 };
	uint8_t write_buffer[BUFFERSIZE] = { 0 };

	HAL_Init();

	SystemClock_Config();

	MX_GPIO_Init();
	MX_USART2_UART_Init();

	while (1) {
		if (ready) {

			//Gets operation from terminal
			HAL_UART_Transmit(&huart2,
					(uint8_t*) "Operation [w/write or r/read]: ", 31, 500);
			HAL_UART_Receive_IT(&huart2, (uint8_t*) uart_operation_buffer,
					sizeof(uart_operation_buffer));

			ready = 0;

		} else if (uart_callback) {

			HAL_UART_Transmit(&huart2, (uint8_t*) uart_operation_buffer,
					sizeof(uart_operation_buffer), 500);
			HAL_UART_Transmit(&huart2, (uint8_t*) "\n\r", 2, 500);

			if (uart_operation_buffer[0] == 'r'
					|| uart_operation_buffer[0] == 'R') {
				read = 1;
			} else if (uart_operation_buffer[0] == 'w'
					|| uart_operation_buffer[0] == 'W') {
				write = 1;
			} else {
				HAL_UART_Transmit(&huart2, (uint8_t*) "Invalid option!", 15,
						500);
				HAL_UART_Transmit(&huart2, (uint8_t*) "\n\r", 2, 500);

				ready = 1;
			}
			uart_callback = 0;
		}

		if (read) {
			//Gets address from terminal
			HAL_UART_Transmit(&huart2, (uint8_t*) "Address: ", 9, 500);
			HAL_UART_Receive(&huart2, (uint8_t*) uart_address_buffer,
					sizeof(uart_address_buffer), 5000);

			HAL_UART_Transmit(&huart2, (uint8_t*) uart_address_buffer,
					sizeof(uart_address_buffer), 500);
			HAL_UART_Transmit(&huart2, (uint8_t*) "\n\r", 2, 500);

			address_buffer[0] = (uint8_t) atoi(uart_address_buffer);

			//Bus read operation
			read_buffer[0] = uc_read_from_bus(address_buffer);

			itoa((uint8_t) read_buffer[0], uart_data_buffer, 10);

			HAL_UART_Transmit(&huart2, (uint8_t*) "Data: ", 6, 500);
			HAL_UART_Transmit(&huart2, (uint8_t*) uart_data_buffer,
					sizeof(uart_data_buffer), 500);
			HAL_UART_Transmit(&huart2, (uint8_t*) "\n\r", 2, 500);

			read = 0;
			ready = 1;

			//Clear uart buffers
			for (int i = 0; i < UARTBUFFERSIZE; i++) {
				uart_address_buffer[i] = 0;
				uart_data_buffer[i] = 0;
			}
		} else if (write) {
			//Gets address from terminal
			HAL_UART_Transmit(&huart2, (uint8_t*) "Address: ", 9, 500);
			HAL_UART_Receive(&huart2, (uint8_t*) uart_address_buffer,
					sizeof(uart_address_buffer), 5000);

			HAL_UART_Transmit(&huart2, (uint8_t*) uart_address_buffer,
					sizeof(uart_address_buffer), 500);
			HAL_UART_Transmit(&huart2, (uint8_t*) "\n\r", 2, 500);

			address_buffer[0] = (uint8_t) atoi(uart_address_buffer);

			//Gets data from terminal
			HAL_UART_Transmit(&huart2, (uint8_t*) "Data: ", 6, 500);
			HAL_UART_Receive(&huart2, (uint8_t*) uart_data_buffer,
					sizeof(uart_data_buffer), 5000);

			HAL_UART_Transmit(&huart2, (uint8_t*) uart_data_buffer,
					sizeof(uart_data_buffer), 500);
			HAL_UART_Transmit(&huart2, (uint8_t*) "\n\r", 2, 500);

			write_buffer[0] = (uint8_t) atoi(uart_data_buffer);

			//Bus write operation
			uc_write_to_bus(address_buffer, write_buffer);

			write = 0;
			ready = 1;

			//Clears uart buffers
			for (int i = 0; i < UARTBUFFERSIZE; i++) {
				uart_address_buffer[i] = 0;
				uart_data_buffer[i] = 0;
			}
		}
		HAL_Delay(500);
	}
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage 
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOE, GPIO_PIN_8|GPIO_PIN_9|GPIO_PIN_10|GPIO_PIN_11 
                          |GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_8|GPIO_PIN_9|GPIO_PIN_10|GPIO_PIN_11 
                          |GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15, GPIO_PIN_RESET);

  /*Configure GPIO pins : PE8 PE9 PE10 PE11 
                           PE12 PE13 PE14 PE15 */
  GPIO_InitStruct.Pin = GPIO_PIN_8|GPIO_PIN_9|GPIO_PIN_10|GPIO_PIN_11 
                          |GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pin : PB12 */
  GPIO_InitStruct.Pin = GPIO_PIN_12;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : PD8 PD9 PD10 PD11 
                           PD12 PD13 PD14 PD15 */
  GPIO_InitStruct.Pin = GPIO_PIN_8|GPIO_PIN_9|GPIO_PIN_10|GPIO_PIN_11 
                          |GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
