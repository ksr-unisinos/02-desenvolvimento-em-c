# Knowledge Shared Room #02

Data: 06/08/2020, quinta-feira

Horário: 18:30h – 19:00h

Tema: Dicas básicas para desenvolver código limpo em C (para dev. embarcado)

Quem: Cassiano Campes

## Organização

Este repositório possui os materiais utilizados durante a KSR.

Iremos utilizar um código base de exemplo para aplicar dicas de código limpo.
O exemplo é a implementação de uma comunicação serial entre um computador e um microcontrolador.
Este microcontrolador por sua vez se comunica com um FPGA via interface micro-pc de 8-bits.

O código foi desenvolvido no CubeIDE.

O código limpo está localizado na raíz da pasta, com o nome: `codigo_limpo.c`.

![Figura](comunicacao_serial.png)

A apresentação será no formato _hands-on_ onde as dicas são aplicadas em cima do código base.

## Bibliotecas padrão para utilização

- `#include <stdlib.h>` https://www.tutorialspoint.com/c_standard_library/stdlib_h.htm

- `#include <string.h>` https://www.tutorialspoint.com/c_standard_library/string_h.htm

- `#include <stdbool.h>` https://www.cplusplus.com/reference/cstdbool/

- `#include <stdint.h>` https://www.cplusplus.com/reference/cstdint/

- `#include <inttypes.h>` https://www.cplusplus.com/reference/cinttypes/

## Referências

- [SEI CERT](https://wiki.sei.cmu.edu/confluence/display/seccode/SEI+CERT+Coding+Standards)
- [Boas práticas em programação C](https://www.embarcados.com.br/boas-praticas-de-programacao-em-linguagem-c/)
- [Repositório com códigos úteis(ou não)](https://gitlab.com/campescassiano/worksforme)
